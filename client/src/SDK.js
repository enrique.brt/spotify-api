window.onSpotifyWebPlaybackSDKReady = () => {
    const token = 'BQAIyqt6fbSZ9Hpxpubt01l6lmf8iJitbf2vtBsH-w_RW1LjNe4L5Y23Esv0S0_-uY8FZx0AZvm0_bylNEpaSbbbuwQNldeoQzBCJ9rF-rCzjzBdLHi3Lf2JlO_MRok73xR_jrvQMjmocctPdfxk8CPnkMBWXKI9hnU';
    const player = new Spotify.Player({
        name: 'Web Playback SDK Quick Start Player',
        getOAuthToken: cb => { cb(token); }
    });

    // Error handling
    player.addListener('initialization_error', ({ message }) => { console.error(message); });
    player.addListener('authentication_error', ({ message }) => { console.error(message); });
    player.addListener('account_error', ({ message }) => { console.error(message); });
    player.addListener('playback_error', ({ message }) => { console.error(message); });

    // Playback status updates
    player.addListener('player_state_changed', state => { console.log(state); });

    // Ready
    player.addListener('ready', ({ device_id }) => {
        console.log('Ready with Device ID', device_id);
    });

    // Not Ready
    player.addListener('not_ready', ({ device_id }) => {
        console.log('Device ID has gone offline', device_id);
    });

    // Connect to the player!
    player.connect();
};