import React, { Component } from 'react';
import './App.css';
import SpotifyWebApi from "spotify-web-api-js";

const spotifyApi = new SpotifyWebApi();

class App extends Component {
  constructor() {
    super();
    const params = this.getHashParams();
    const token = params.access_token;


    if (token) {
      spotifyApi.setAccessToken(token);
    }
    this.state = {
      loggedIn: token ? true : false,
      nowPlaying: { name: 'Aktuell wird kein Song gespielt', albumArt: '', artist: '' },
      devices: { name: 'Kein gerät', id: '', volumePercentage: 0 }
    }

  }

  getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);
    while (e = r.exec(q)) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
    }
    return hashParams;

  }

  getDevice() {
    spotifyApi.getMyDevices()
      .then((response) => {
        this.setState({
          device: {
            name: response.devices[0].name,
            id: response.devices[0].id,
            volumePercentage: response.devices[0].volume_percent
          }
        });
      })
  }


  getNowPlaying() {
    spotifyApi.getMyCurrentPlaybackState()
      .then((response) => {
        this.setState({
          nowPlaying: {
            name: response.item.name,
            albumArt: response.item.album.images[0].url,
            artist: response.item.artists[0].name,
          }
        });

      })
  }




  render() {
    return (
      <div className="App" >
        {this.getHashParams() ?
          (<div>
            <div className="sidenav">
              <div className="profile">
                <img src="./me.jpg" width="90px" height="90px" className="avatar-pic" alt="Avatar" />
              </div>
              <ul>
                <li><a href="http://localhost:8888">Home</a></li>
                <li><a href="http://localhost:8888">Browse</a></li>
                <li><a href="http://localhost:8888">Album</a></li>
                <li><a href="http://localhost:8888">Artist</a></li>
              </ul>
              <button onClick={() => this.getNowPlaying()} className="btn check-btn">Check Whats playing</button>
            </div>
            <div className="wrapper">
              <div className="hero-wrapper">
                <div className="header">
                  <h1 className="title">Home</h1>
                  <button><a href="https://accounts.spotify.com/de/status">Logout</a></button>
                </div>

                <div className="playlists">
                  <div className="playlist-item" >
                    <img src={this.state.nowPlaying.albumArt} width="200px" alt="Albumart" />
                    <h1>{this.state.nowPlaying.name}</h1>
                  </div>
                  <div className="playlist-item">
                    <img src={this.state.nowPlaying.albumArt} width="200px" alt="Albumart" />
                    <h1>{this.state.nowPlaying.name}</h1>
                  </div>
                  <div className="playlist-item">
                    <img src={this.state.nowPlaying.albumArt} width="200px" alt="Albumart" />
                    <h1>{this.state.nowPlaying.name}</h1>
                  </div>
                  <div className="playlist-item">
                    <img src={this.state.nowPlaying.albumArt} width="200px" alt="Albumart" />
                    <h1>{this.state.nowPlaying.name}</h1>
                  </div>
                </div>
              </div>
            </div>
            <div className="song-card">
              <div className="album-art">
                <img src={this.state.nowPlaying.albumArt} alt="Albumart" className="album-cover" width="60px" />
              </div>
              <div className="song-info">
                <div className="song-label">
                  <h1> {this.state.nowPlaying.name}</h1>
                </div>
                <div className="song-artist">
                  <p>{this.state.nowPlaying.artist}</p>
                </div>
              </div >
              <div className="playing-device">
                <img src="./mockup.png" width="30px" className="mockup" alt="Device" />
                <div className="device-information">
                  <h1>{this.state.devices.name}</h1>
                  <p>connected</p>
                </div>
              </div>
            </div>
          </div>
          ) :
          (<div>
            <a href="http://localhost:8888">
              <button className=" btn login-btn">Login With Spotify</button>
            </a >
          </div>)
        }
      </div>
    );
  }
}

export default App;
